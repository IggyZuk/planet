using System;
using System.Collections.Generic;

public class Planet
{
    public Dictionary<Guid, Vertex> vertices = new Dictionary<Guid, Vertex>();
    public Dictionary<Guid, Edge> edges = new Dictionary<Guid, Edge>();
    public Vector center = new Vector();
}
