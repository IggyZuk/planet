using System;

public class Vertex
{
    public Guid id = Guid.NewGuid();
    public Vector pos = new Vector();
    public Vector norm = new Vector(0f, 1f);
}
