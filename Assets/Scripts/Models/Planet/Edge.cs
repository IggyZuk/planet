using System;

public class Edge
{
    public Guid id = Guid.NewGuid();
    public Guid fromVertexId = new Guid();
    public Guid toVertexId = new Guid();
    public Vector line = new Vector();
    public Vector norm = new Vector();
}
