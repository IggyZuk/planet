using System;

public class Actor
{
    public Guid id = Guid.NewGuid();
    public Vector pos = new Vector();
    public Vector vel = new Vector();
    public Vector norm = new Vector(0f, 1f);

    public Vector localMoveDir = new Vector();
}
