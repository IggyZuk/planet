using System;
using System.Collections.Generic;

public class World
{
    public int tick = 0;
    public Planet planet = new Planet();
    public Dictionary<Guid, Actor> actors = new Dictionary<Guid, Actor>();

    public List<Vector> gizmos = new List<Vector>();
}
