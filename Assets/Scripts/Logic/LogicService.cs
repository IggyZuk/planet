using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class LogicService
{
    public static void Tick(World world)
    {
        world.tick++;

        foreach (Actor actor in world.actors.Values)
        {
            actor.pos += actor.vel * 0.1f;
            actor.vel *= 0.9f;

            GravityInfo gravity = RaycastLogic.FindGravity(world, actor.pos);

            if (gravity.isInside)
            {
                actor.pos = gravity.point;
                actor.vel = Vector.Project(actor.vel, Vector.RotateLeft(gravity.norm)) + Vector.Project(-actor.vel, gravity.norm);
            }
            else
            {
                Vector gravityVector = gravity.point - actor.pos;
                actor.vel += gravityVector.Normalize() * 0.1f;
                //actor.vel += Vector.RotateRight(actor.norm.Normalize()) * 0.01f;
            }
        }
    }

    public static void AddActor(World world, Vector2 clickPos, Actor actor)
    {
        actor.pos = new Vector(clickPos);
        actor.vel = new Vector(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        actor.norm = new Vector(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).Normalize();
        world.actors.Add(actor.id, actor);
    }

    public static List<Edge> GetFacingEdges(Vector norm, List<Edge> edges)
    {
        List<Edge> facingEdges = new List<Edge>();

        foreach (var e in edges)
        {
            if (Vector.Dot(e.norm, norm) < 0f) facingEdges.Add(e);
        }

        return facingEdges;
    }

    public static void RotatePlanet(World world, float angle)
    {
        for (int i = 0; i < world.planet.vertices.Count; i++)
        {
            world.planet.vertices.Values.ToList()[i].pos = Vector.RotateAngle(world.planet.vertices.Values.ToList()[i].pos, angle);
        }
        CalculateEdgeNormals(world);
    }

    public static void CalculateEdgeNormals(World world)
    {
        foreach (var e in world.planet.edges.Values)
        {
            Vertex fromVertex = world.planet.vertices[e.fromVertexId];
            Vertex toVertex = world.planet.vertices[e.toVertexId];

            e.line = fromVertex.pos - toVertex.pos;
            e.norm = Vector.RotateLeft(e.line).Normalize();

        }

        List<Edge> edges = world.planet.edges.Values.ToList();
        for (int i = 0; i <= world.planet.edges.Count - 1; i++)
        {
            int nextEdgeIndex = i < world.planet.edges.Count - 1 ? i + 1 : 0;

            Edge e1 = edges[i];
            Edge e2 = edges[nextEdgeIndex];

            Vertex vertex = world.planet.vertices[e2.fromVertexId];
            vertex.norm = (e1.norm + e2.norm).Normalize();
        }
    }

    public static World BuildWorld()
    {
        World w = new World();
        w.planet = BuildPlanet(17, 4f, 0.75f);
        CalculateEdgeNormals(w);
        return w;
    }

    public static Planet BuildPlanet(int vertexCount, float radius, float randomOffset)
    {
        Planet p = new Planet();

        for (int i = 0; i < vertexCount; i++)
        {
            float percent = (float)i / vertexCount;
            float angle = percent * (Mathf.PI * 2);

            Vertex v = new Vertex();
            v.pos = new Vector(Mathf.Cos(angle), Mathf.Sin(angle)) * radius;

            v.pos += new Vector(
                Random.Range(-randomOffset, randomOffset),
                Random.Range(-randomOffset, randomOffset)
            );

            p.vertices.Add(v.id, v);
        }

        for (int i = 0; i <= p.vertices.Count - 1; i++)
        {
            int nextVertexIndex = i < p.vertices.Count - 1 ? i + 1 : 0;

            Edge e = new Edge();
            e.fromVertexId = p.vertices.Keys.ToList()[i];
            e.toVertexId = p.vertices.Keys.ToList()[nextVertexIndex];
            p.edges.Add(e.id, e);
        }

        return p;
    }
}
