using System;
using System.Collections.Generic;
using UnityEngine;

public class RaycastInfo
{
    public bool isHit = false;
    public Vector hitPos = new Vector();
}

public class EdgeInfo
{
    public bool isHit = false;
    public Vector hitPos = new Vector();
    public Guid edgeId = new Guid();
    public float distanceSq = 0f;
}

public class VertexInfo
{
    public bool isHit = false;
    public Guid vertexId;
    public float distanceSq = 0f;
}

public class CollisionInfo
{
    public bool isHit = false;
    public Vector hitPos = new Vector();
    public float distanceSq = 0f;
}

public class GravityInfo
{
    public Vector point = new Vector();
    public Vector norm = new Vector();
    public bool isInside = false;
}

public static class RaycastLogic
{
    public static RaycastInfo LineIntersect(Vector a, Vector b, Vector c, Vector d)
    {
        Vector r = (b - a); // edge (a, b)
        Vector s = (d - c); // edge (c, d)

        float f = Vector.Cross(r, s);

        float u = ((c.x - a.x) * r.y - (c.y - a.y) * r.x) / f;
        float t = ((c.x - a.x) * s.y - (c.y - a.y) * s.x) / f;

        RaycastInfo result = new RaycastInfo();
        result.isHit = false;

        if (-0.01f <= u && u <= 1.01f && -0.01f <= t && t <= 1.01f)
        {
            result.isHit = true;
            result.hitPos = a + r * t;
        }

        return result;
    }

    public static GravityInfo FindGravity(World world, Vector point)
    {
        GravityInfo result = new GravityInfo();

        EdgeInfo edgeInfo = FindClosestEdgeToPoint(world, point);
        if (edgeInfo.isHit)
        {
            // Make sure the edge is pointing out of the planet
            Edge edge = world.planet.edges[edgeInfo.edgeId];

            result.point = edgeInfo.hitPos;
            result.norm = edge.norm;

            Vertex fromVertex = world.planet.vertices[edge.fromVertexId];
            Vertex toVertex = world.planet.vertices[edge.toVertexId];

            Vector halfwayPoint = Vector.Lerp(fromVertex.pos, toVertex.pos, 0.5f);
            Vector edgeToPoint = point - halfwayPoint;

            if (Vector.Dot(edgeToPoint, edge.norm) > 0)
            {
                return result;
            }
        }

        // Since we haven't returned yet, we haven't found an edge
        VertexInfo vertexInfo = FindClosestVertex(world, point);

        Vertex vertex = world.planet.vertices[vertexInfo.vertexId];

        Vector vertexToPoint = point - vertex.pos;

        if (Vector.Dot(vertexToPoint, vertex.norm) > 0)
        {
            result.point = vertex.pos;
            result.norm = vertex.norm;
            return result;
        }

        result.isInside = true;

        if (edgeInfo.isHit && edgeInfo.distanceSq < vertexInfo.distanceSq)
        {
            Edge edge = world.planet.edges[edgeInfo.edgeId];

            result.point = edgeInfo.hitPos;
            result.norm = edge.norm;
        }
        else
        {
            result.point = vertex.pos;
            result.norm = vertex.norm;
        }

        return result;
    }

    public static EdgeInfo FindClosestEdgeToPoint(World world, Vector point)
    {
        EdgeInfo result = new EdgeInfo();

        float closestDistanceSq = float.MaxValue;
        Edge closestEdge = null;

        foreach (Edge e in world.planet.edges.Values)
        {
            Vertex fromVertex = world.planet.vertices[e.fromVertexId];
            Vertex toVertex = world.planet.vertices[e.toVertexId];

            CollisionInfo collisionInfo = DistToSegmentSq(
                point,
                fromVertex.pos,
                toVertex.pos
            );

            if (collisionInfo.distanceSq < closestDistanceSq && collisionInfo.distanceSq > 0f)
            {
                closestDistanceSq = collisionInfo.distanceSq;
                closestEdge = e;

                result.isHit = true;
                result.hitPos = collisionInfo.hitPos;
                result.edgeId = closestEdge.id;
                result.distanceSq = closestDistanceSq;
            }
        }

        return result;
    }

    public static float FindDistanceToLineSegment(Vector p, Vector l1, Vector l2)
    {
        float A = p.x - l1.x;
        float B = p.y - l1.y;
        float C = l2.x - l1.x;
        float D = l2.y - l1.y;

        float dot = A * C + B * D;
        float len_sq = C * C + D * D;
        float param = dot / len_sq;

        float xx, yy;

        if (param < 0 || (l1.x == l2.x && l1.y == l2.y))
        {
            xx = l1.x;
            yy = l1.y;
        }
        else if (param > 1)
        {
            xx = l2.x;
            yy = l2.y;
        }
        else
        {
            xx = l1.x + param * C;
            yy = l1.y + param * D;
        }

        float dx = p.x - xx;
        float dy = p.y - yy;

        return Mathf.Sqrt(dx * dx + dy * dy);
    }

    static float Sq(float x) { return x * x; }
    static float DistSq(Vector v, Vector w) { return Sq(v.x - w.x) + Sq(v.y - w.y); }
    static CollisionInfo DistToSegmentSq(Vector p, Vector v, Vector w)
    {
        CollisionInfo result = new CollisionInfo();

        float l2 = DistSq(v, w);

        if (l2 == 0f)
        {
            result.distanceSq = DistSq(p, v);
            return result;
        }

        float t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2;

        if (t < 0f || t > 1f)
        {
            return result;
        }

        t = Mathf.Max(0f, Mathf.Min(1f, t));

        Vector r = new Vector(
            v.x + t * (w.x - v.x),
            v.y + t * (w.y - v.y)
        );

        result.distanceSq = DistSq(p, r);
        result.hitPos = r;
        result.isHit = true;

        return result;
    }
    //static CollisionInfo DistToSegment(Vector p, Vector v, Vector w) { return Mathf.Sqrt(DistToSegmentSq(p, v, w)); }

    public static VertexInfo FindClosestVertex(World world, Vector point)
    {
        VertexInfo result = new VertexInfo();

        float closestDistanceSq = float.MaxValue;
        Vertex closestVertex = null;

        foreach (var v in world.planet.vertices.Values)
        {
            float distSq = (v.pos - point).MagnitudeSq();
            if (distSq < closestDistanceSq)
            {
                closestDistanceSq = distSq;
                closestVertex = v;

                result.vertexId = closestVertex.id;
                result.distanceSq = closestDistanceSq;
            }
        }

        return result;
    }
}
