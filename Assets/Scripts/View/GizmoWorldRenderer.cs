using UnityEngine;

public class GizmoWorldRenderer
{
    public static void Render(World world)
    {
        foreach (var v in world.planet.vertices.Values)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(v.pos.Vector2(), 0.1f);
            Gizmos.color = Color.black;
            Gizmos.DrawRay(v.pos.Vector2(), v.norm.Vector2() * 0.5f);
        }

        foreach (var e in world.planet.edges.Values)
        {
            Vertex fromVertex = world.planet.vertices[e.fromVertexId];
            Vertex toVertex = world.planet.vertices[e.toVertexId];

            Gizmos.color = Color.red;
            Gizmos.DrawLine(fromVertex.pos.Vector2(), toVertex.pos.Vector2());

            Gizmos.color = Color.magenta;
            Vector halfwayPos = Vector.Lerp(fromVertex.pos, toVertex.pos, 0.5f);
            Gizmos.DrawRay(halfwayPos.Vector2(), e.norm.Vector2() * 0.25f);
        }

        foreach (var actor in world.actors.Values)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(actor.pos.Vector2(), 0.1f);

            Gizmos.color = Color.yellow;
            Gizmos.DrawRay(actor.pos.Vector2(), actor.vel.Vector2() * 0.3f);

            Gizmos.color = Color.white;
            Gizmos.DrawRay(actor.pos.Vector2(), actor.norm.Vector2() * 0.3f);
        }

        Gizmos.color = Color.cyan;
        foreach (var g in world.gizmos)
        {
            Gizmos.DrawCube(g.Vector2(), Vector3.one * 0.1f);
        }
    }
}
