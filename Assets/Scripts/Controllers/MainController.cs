﻿using UnityEngine;

public class MainController : MonoBehaviour
{
    World world;

    void Awake()
    {
        world = LogicService.BuildWorld();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            for (int i = 0; i < (Input.GetKey(KeyCode.M) ? 16 : 1); i++)
            {
                LogicService.AddActor(
                    world,
                    Camera.main.ScreenToWorldPoint(Input.mousePosition),
                    new Actor()
                );
            }
        }

        Vector mousePos = new Vector((Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition));

        world.gizmos.Add(mousePos);

        var gravity = RaycastLogic.FindGravity(world, mousePos);
        Debug.DrawLine(mousePos.Vector2(), gravity.point.Vector2(), gravity.isInside ? Color.red : Color.green);

        if (Input.GetKeyDown(KeyCode.R))
        {
            world = LogicService.BuildWorld();
        }

    }

    void FixedUpdate()
    {
        world.gizmos.Clear();

        for (int i = 0; i < (Input.GetKey(KeyCode.T) ? 16 : 1); i++)
        {
            LogicService.Tick(world);
            //LogicService.RotatePlanet(world, Mathf.PI / 360f);
        }
    }

    void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            if (world != null)
            {
                GizmoWorldRenderer.Render(world);
            }
        }
    }
}
